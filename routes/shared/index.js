const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('shared/legal/cookies-policy', {
    title: 'Cookies Policy',
    description: '',
    path: req.originalUrl
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const axios = require('axios')

/* Get JWT from Strapi  */
router.post('/', async (req, res) => {
  const { email, password } = req.body.user

  try {
    const { data } = await axios.post(`${process.env.EOS_STRAPI_SERVER_DEV}/auth/local`, {
      identifier: email,
      password
    })

    const { status, user: { _id } } = data

    if (status === 'Authenticated') {
      req.session.user = _id

      return res.send('ok').status(200)
    }
  } catch (err) {
    console.log(err)
    res.send('error').status(400)
  }
})

module.exports = router

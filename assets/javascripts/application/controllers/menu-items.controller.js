$(function () {
  /*
    Use this as follows:
    Add the class .js-select-current to the link you want to select
    it will check if `href = path` matches with the current path
    in the URL, and if it does, the item will get the class `.selected`
   */

  $('.js-select-current').addClass(function () {
    const itemRoute = $(this).attr('href')
    const itemRouteNew = window.location.pathname
    const itemRouteSplit = itemRouteNew.split('/')

    if (itemRouteSplit.length > 3) {
      itemRouteSplit.pop()
    }
    const windowLocation = itemRouteSplit.join('/')
    return windowLocation === itemRoute ? 'selected' : ''
  })

  $('.js-select-submenu').addClass(function () {
    const itemRoute = $(this).attr('href')
    return window.location.pathname === itemRoute ? 'selected' : ''
  })
  /*
    Use this as follows:
    Add the class .js-select-current-parent to the link you want to select.
    It will check if the `href = path` in your item
    matches with parent section of the current path. For example:
    being located at localhost:3000/foo/bar, if your href is `/foo`
    the item will get the class `.selected`, but if the href was `/foo/bar`
    then it will be skipped and not be selected.
   */
  $('.js-select-current-parent').addClass(function () {
    const itemRoute = $(this).attr('href').replace('/', '')
    const currentRoute = window.location.pathname
    const currentRouteParent = currentRoute.split('/')
    return itemRoute === currentRouteParent[1] ? 'selected' : ''
  })

  makeSubmenuSectionVisible(submenuCollapse, submenuDropdownPosition) // eslint-disable-line no-undef
  $(window).resize(fn => {
    makeSubmenuSectionVisible(submenuCollapse, submenuDropdownPosition)// eslint-disable-line no-undef
  })

  // Call the dropdown function
  selectedDropdown()
  selectedDropdownMobile()
})

/*
  Use this as follows:
  Add the class .js-submenu-make-visible to the submenu nav you want to make visible.
  It will check if the `data-parent-menu = path` in the submenu
  matches with parent section of the current path. For example:
  being located at localhost:3000/foo/bar, if your data-parent-menu is `/foo`
  the submenu will become visible, but if the href was `/foo/bar`
  then it will stay hidden.
 */

const makeSubmenuSectionVisible = (submenu, submenuDopDown) => {
  $('.js-submenu-make-visible').addClass(function () {
    const pm = $(this).data('parent-menu') // pm => parent menu
    const currentRoute = window.location.pathname
    const currentRouteParent = currentRoute.split('/')
    const pmSplit = pm.split('/')
    submenuDopDown()
    submenu()

    const newCurrentParent = currentRouteParent.slice(1, 3).join('/')

    if (currentRouteParent.length > 2 && pmSplit.length > 1) {
      return pm === newCurrentParent ? 'visible js-submenu-visible' : ''
    } else {
      return pm === currentRouteParent[1] ? 'visible js-submenu-visible' : ''
    }
  })
}

/* Find if an element inside the menu dropdown list is active and leave the dropdown open */
const selectedDropdown = () => {
  /* Big screen dropdown open */
  const $selectedSub = $('.menu-dropdown-list').find('a.selected')[0]
  $($selectedSub).parents('.menu-dropdown').addClass('selected')
  $($selectedSub).closest('.menu-dropdown-list').siblings('.js-dropdown-toggle').prop('checked', true)
}

const selectedDropdownMobile = () => {
  /* Mobile dropdown open */
  const $menuDropdownMobile = $('.js-mobile-menu .menu-dropdown')
  const $selectedSubMobile = $('.js-mobile-menu .menu-dropdown').find('.selected')[0]

  for (let i = 0; i < $menuDropdownMobile.length; i++) {
    $($selectedSubMobile).parents('.menu-dropdown').addClass('selected')
    $($menuDropdownMobile[i]).find('input').attr('id', `internal-tools-toggle-mobile-${i}`)
    $($menuDropdownMobile[i]).find('label').attr('for', `internal-tools-toggle-mobile-${i}`)
    $($selectedSubMobile).closest('.menu-dropdown-list').siblings('.js-dropdown-toggle').prop('checked', true)
  }
}

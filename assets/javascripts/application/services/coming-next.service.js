const getComingNext = (callback) => { // eslint-disable-line no-unused-vars
  const query = `comingnexts(where: {enabled:true}) { title, description, image}`

  $.when(
    $.ajax({
      url: `/api/strapi?q=${query}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`there was an error retrieving the data`)
      }
    }))
    .then(function (data) {
      callback(data)
    })
}

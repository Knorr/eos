describe("menu", () => {
  before(() => {
    cy.setCookie('annoucement', '{%22wasShown%22:true%2C%22campaing%22:%22SUSE%20rebrand%22}')
    cy.viewport(1336, 750)
    cy.visit('/')
  })

  it("should close/open on click", () => {
    cy.get('.js-main-menu').should('not.have.class', 'collapsed-sidebar')
    cy.get('.js-sidebar-toggle').click()
    cy.get('.js-main-menu').should('have.class', 'collapsed-sidebar')
    cy.get('.js-sidebar-toggle').click()
    cy.get('.js-main-menu').should('not.have.class', 'collapsed-sidebar')
  })

  it('should be closed on small screen', () => {
    cy.viewport(980, 750)
    cy.get('.js-main-menu').should('have.class', 'collapsed-sidebar')
  })
})
